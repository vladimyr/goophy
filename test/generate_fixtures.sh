#!/bin/sh
echo '' | nc mozz.us 7005 > test/fixtures/mozz.us_root.txt
echo '/demo-links' | nc mozz.us 7005 > test/fixtures/mozz.us_menu_link_gallery.txt
echo '/files/bofh1.txt' | nc mozz.us 7005 > test/fixtures/mozz.us_example.txt
echo '/files/hello.bin' | nc mozz.us 7005 > test/fixtures/mozz.us_example.bin
echo '/files/linux.jpg' | nc mozz.us 7005 > test/fixtures/mozz.us_example.jpg
