'use strict';

const { simulator } = require('now-we-test');
const func = require('../api/proxy');
const path = require('path');
const readFile = require('util').promisify(require('fs').readFile);
const test = require('tape');

const readFixture = (filename, encoding) => readFile(path.join(__dirname, filename), encoding);

const handler = simulator(func);

test('fetch root gopher menu', async t => {
  const url = 'gopher://mozz.us:7005';
  const resp = await handler.get('/').query({ raw: true, url });
  t.comment(`url=${url}`);
  t.comment(`status=${resp.status}`);
  t.plan(4);
  t.notEquals(resp.get('Transfer-Encoding'), 'chunked', 'response is buffered');
  t.equals(resp.type, 'text/plain', 'response type is `text/plain`');
  t.ok(resp.ok, 'status code is 200');
  const expected = await readFixture('./fixtures/mozz.us_root.txt', 'utf-8');
  t.equals(resp.text, expected);
});

test('fetch example gopher menu', async t => {
  const url = 'gopher://mozz.us:7005/1/demo-links';
  const resp = await handler.get('/').query({ raw: true, url });
  t.comment(`url=${url}`);
  t.comment(`status=${resp.status}`);
  t.plan(4);
  t.notEquals(resp.get('Transfer-Encoding'), 'chunked', 'response is buffered');
  t.equals(resp.type, 'text/plain', 'response type is `text/plain`');
  t.ok(resp.ok, 'status code is 200');
  const expected = await readFixture('./fixtures/mozz.us_menu_link_gallery.txt', 'utf-8');
  t.equals(resp.text, expected);
});

test('fetch text document', async t => {
  const url = 'gopher://mozz.us:7005/0/files/bofh1.txt';
  const resp = await handler.get('/').query({ raw: true, url });
  t.comment(`url=${url}`);
  t.comment(`status=${resp.status}`);
  t.plan(4);
  t.notEquals(resp.get('Transfer-Encoding'), 'chunked', 'response is buffered');
  t.equals(resp.type, 'text/plain', 'response type is `text/plain`');
  t.ok(resp.ok, 'status code is 200');
  const expected = await readFixture('./fixtures/mozz.us_example.txt', 'utf-8');
  t.equals(resp.text, expected);
});

test('fetch binary', async t => {
  const url = 'gopher://mozz.us:7005/9/files/hello.bin';
  const resp = await handler.get('/').query({ raw: true, url });
  t.comment(`url=${url}`);
  t.comment(`status=${resp.status}`);
  t.plan(3);
  t.equals(resp.get('Transfer-Encoding'), 'chunked', 'response is streamed');
  t.equals(resp.type, 'application/octet-stream', 'response type is `application/octet-stream`');
  t.ok(resp.ok, 'status code is 200');
  // const expected = await readFixture('./fixtures/mozz.us_example.bin');
  // t.equals(resp.body.toString(), expected);
});

test('fetch image', async t => {
  const url = 'gopher://mozz.us:7005/I/files/linux.jpg';
  const resp = await handler.get('/').query({ raw: true, url });
  t.comment(`url=${url}`);
  t.comment(`status=${resp.status}`);
  t.plan(3);
  t.equals(resp.get('Transfer-Encoding'), 'chunked', 'response is streamed');
  t.equals(resp.type, 'image/jpeg', 'response type is `image/jpeg`');
  t.ok(resp.ok, 'status code is 200');
  // const expected = await readFixture('./fixtures/mozz.us_example.jpg');
  // t.equals(resp.body.toString(), expected);
});
