import './style.styl';
import { getMeta } from './helpers';
import Menu from './menu';
const GopherType = require('../lib/gopher/type');
const GopherUrl = require('../lib/gopher/url');

const reEnd = /\r?\n(?:\.\r?\n)?$/;

const noscript = document.querySelector('noscript');
const gopherUrl = window.gopherLocation = new GopherUrl(getMeta('gopher-url'));

const spacer = createSpacer(80);
document.body.appendChild(spacer);
document.body.style.minWidth = spacer.offsetWidth + 'px';

if (gopherUrl.type !== GopherType.Menu) {
  const content = document.createElement('pre');
  content.className = 'content';
  content.textContent = noscript.textContent.replace(reEnd, '');
  document.body.appendChild(content);
} else {
  const menu = new Menu(noscript.textContent);
  const content = document.createElement('div');
  document.body.appendChild(content);
  content.innerHTML = menu.render();
}

function createSpacer(size = 80) {
  const cols = Array.from({ length: size }, () => '&nbsp;');
  const innerHTML = cols.join('');
  const pre = document.createElement('pre');
  pre.className = 'spacer';
  pre.innerHTML = innerHTML;
  return pre;
}
