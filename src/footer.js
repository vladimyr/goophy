const noop = Function.prototype;
const reFooter = /^\s*_{60,}\r?\n\s*(?:Gophered by|Served by)/;
const reServerInfo = /by(\s+)(.*?)(?=\s|$)/;

const anchor = (label, href, className) => {
  return `
    <a
      class="${className}"
      rel="norefferer noopener"
      target="_blank"
      href="${href}">
      ${label}
    </a>`.trim();
};

const Servers = {
  gophernicus: 'https://github.com/prologic/gophernicus',
  'flask-gopher': 'https://github.com/michael-lazar/flask-gopher'
};

export const isFooter = ({ title = '' } = {}) => reFooter.test(title);

export function processFooter(entry) {
  entry.className = 'footer';
  entry.raw = true;
  entry.title = entry.title.replace(reServerInfo, (_, space, serverInfo) => {
    const href = devpage(Servers, serverInfo) || '#';
    return `by${space}${anchor(serverInfo, href, 'server-info')}`;
  });
}

function devpage(servers, serverInfo) {
  return find(servers, (url, name) => {
    const re = new RegExp(`^${name}`, 'i');
    return re.test(serverInfo);
  });
}

function find(obj = {}, fn = noop) {
  const key = Object.keys(obj).find(key => fn(obj[key], key));
  return key && obj[key];
}
