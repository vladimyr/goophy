import { getMeta } from './helpers';
const GopherType = require('../lib/gopher/type');
const GopherUrl = require('../lib/gopher/url');

const gopherUrl = new GopherUrl(getMeta('gopher-url'));

const isRoot = selector => selector === '' || selector === '/';

const Icons = {
  [GopherType.Text]: 'insert_drive_file',
  [GopherType.Document]: 'insert_drive_file',
  [GopherType.Menu]: 'folder',
  [GopherType.Image]: 'image',
  [GopherType.Gif]: 'image',
  [GopherType.Hyperlink]: 'link'
};

export default function icon({ host, port, selector, type } = {}) {
  if (!type) return '';
  if (type === GopherType.Menu && isHomeLink({ host, port, selector })) {
    return 'home';
  }
  return Icons[type] || '';
}

function isHomeLink({ hostname, port, selector }) {
  return !isExternal({ hostname, port }) && isRoot(selector);
}

function isExternal({ hostname, port }) {
  if (hostname !== gopherUrl.hostname) return false;
  if (!port && !gopherUrl.port) return true;
  return port === gopherUrl.port;
}
