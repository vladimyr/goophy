import { escape, getClassName, splitColumns, startsWith } from './helpers';
import { isFooter, processFooter } from './footer';
import icon from './icon';
import link from './link';
const GopherType = require('../lib/gopher/type');

const reMenuEnd = /\r?\n(?:\.\r?\n)?$/;
const reLines = /\r?\n/g;
const delim = '\t';

class MenuEntry {
  constructor(text) {
    Object.assign(this, parseEntry(text));
  }

  equals(other = {}) {
    return this.type === other.type &&
      this.selector === other.selector &&
      this.host === other.host &&
      this.port === other.port;
  }

  render() {
    const label = this.type && this.type.label;

    if (this.type === GopherType.Ignore) {
      const className = getClassName(this.selector.toLowerCase(), this.className);
      const title = this.raw ? this.title : escape(this.title);
      return `<li class="item ${label}">
        <pre class="${className}">${title}</pre>
      </li>`;
    }

    return `<li class="item ${label}">
      <a href="${link(this)}">
        <i class="icon material-icons-sharp">${icon(this)}</i>
        <span class="label">${escape(this.title)}</span>
      </a>
    </li>`;
  }
}

export default class Menu {
  constructor(text) {
    const entries = parseMenu(text);
    this.entries = mergeEntries(entries);
    this._stripMargin();
    this._processFooter();
  }

  _stripMargin(char = 'i') {
    this.entries
      .filter(it => it.type === GopherType.Ignore)
      .forEach(it => stripMargin(it, char));
  }

  _processFooter() {
    this.entries
      .filter(it => (it.type === GopherType.Ignore) && isFooter(it))
      .forEach(it => processFooter(it));
  }

  render({ className = '' } = {}) {
    const innerHtml = this.entries.map(it => it.render()).join('');
    return `<ul class="menu ${className}">${innerHtml}</ul>`;
  }
}

function parseMenu(text) {
  text = text.replace(reMenuEnd, '');
  const lines = text.split(reLines);
  return lines
    .filter(it => it.length > 0)
    .map(it => new MenuEntry(it));
}

function parseEntry(text) {
  const [prefix, selector, host, tail] = splitColumns(text, 4, delim);

  // Ignore Gopher+
  let [port] = tail.split(delim);
  port = parseInt(port, 10) || 70;
  const type = GopherType.fromCode(prefix[0]);
  const title = prefix.substring(1) || ' ';
  return {
    title,
    type,
    selector,
    host,
    port
  };
}

function mergeEntries(entries) {
  return entries.reduce((acc, entry) => {
    const last = acc[acc.length - 1];
    if (entry.equals(last)) {
      last.title += '\n' + entry.title;
      return acc;
    }
    acc.push(entry);
    return acc;
  }, []);
}

function stripMargin(entry, char = 'i') {
  if (entry.title[0] !== char) return;
  const lines = entry.title.split(/\r?\n/g);
  if (!startsWith(lines, char)) return;
  entry.title = lines.map(line => line.substring(1) || ' ').join('\n');
}
