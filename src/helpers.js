const trim = (str = '') => str.trim();

export function getClassName(...classes) {
  return classes.reduce((acc, cls) => {
    cls = trim(cls);
    if (!cls) return acc;
    if (!acc) return cls;
    acc += ' ' + cls;
    return acc;
  }, '');
}

export function escape(input) {
  return input
    .replace(/&/g, '&amp;')
    .replace(/"/g, '&quot;')
    .replace(/'/g, '&#39;')
    .replace(/</g, '&lt;')
    .replace(/>/g, '&gt;');
}

export function getMeta(name) {
  if (!name) return;
  const meta = document.querySelector(`meta[name="${name}"]`);
  return meta.getAttribute('value');
}

export function splitColumns(line, count, delim = '\t') {
  const columns = [];
  let i = 0;
  let lastIndex;
  while (i < count && (lastIndex = line.indexOf(delim)) !== -1) {
    const column = line.substring(0, lastIndex);
    columns.push(column);
    line = line.substring(lastIndex + delim.length);
    i += 1;
  }
  if (line) columns.push(line);
  while (columns.length < count) columns.push('');
  return columns;
}

export function startsWith(lines, prefix) {
  const margin = Array.from(new Set(
    lines.map(line => line.substring(0, prefix.length))
  ));
  return margin.length === 1 && margin[0] === prefix;
}
