import path from 'path';
const GopherType = require('../lib/gopher/type');
const isUrl = str => /^https?:\/\//.test(str);

export default function link({ host, port = 70, selector, type } = {}) {
  if (type === GopherType.Hyperlink) {
    const url = selector.replace(/^URL:/, '');
    if (isUrl(url)) return url;
  }
  let hostname = host;
  if (port !== 70) hostname += `:${port}`;
  return `/gopher://${path.join(`${hostname}`, `${type}${selector}`)}`;
}
