'use strict';

const { createConnection } = require('net');
const debug = require('debug')('gopher');
const getStream = require('get-stream');
const GopherUrl = require('./url');

const getter = fn => ({ get: fn, configurable: false, enumerable: true });

class GopherResponse {
  constructor(socket, url) {
    this._socket = socket;
    Object.defineProperties(this, {
      url: getter(() => url),
      type: getter(() => url.type),
      body: getter(() => this._socket)
    });
  }

  buffer() {
    return getStream.buffer(this._socket);
  }

  text() {
    return getStream(this._socket);
  }
}

module.exports = async function fetch(url) {
  url = new GopherUrl(url);
  debug('url: %j', url);
  const socket = await connect(url.hostname, url.port);
  socket.write(url.selector);
  socket.write('\r\n');
  return new GopherResponse(socket, url);
};

function connect(host, port = 70) {
  debug('connect to: %s %d', host, port);
  return new Promise((resolve, reject) => {
    const socket = createConnection(port, host);
    socket.once('connect', () => resolve(socket));
    socket.once('error', err => reject(err));
  });
}
