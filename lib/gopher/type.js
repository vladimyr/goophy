'use strict';

const noop = Function.prototype;

class GopherType {
  constructor(code, label) {
    this._code = code;
    this._label = label;
  }

  get code() {
    return this._code;
  }

  get label() {
    return this._label;
  }

  [Symbol.toPrimitive]() {
    return this.code;
  }

  equals(type) {
    return this.code === type.code;
  }

  toString() {
    return this.code;
  }

  toJSON() {
    return this.code;
  }

  static isImage(type) {
    return type === this.Image || type === this.Gif;
  }

  static fromCode(code = Types.Menu.code) {
    return find(Types, type => type.code === code);
  }
}

const Types = {
  Text: new GopherType('0', 'text'),
  Menu: new GopherType('1', 'menu'),
  Hyperlink: new GopherType('h', 'hyperlink'),
  Image: new GopherType('I', 'image'),
  Gif: new GopherType('g', 'gif'),
  Document: new GopherType('d', 'document'),
  Binary: new GopherType('9', 'binary'),
  Ignore: new GopherType('i', 'ignore')
};

module.exports = Object.assign(GopherType, Types);

function find(obj = {}, fn = noop) {
  const key = Object.keys(obj).find(key => fn(obj[key], key));
  if (key) return obj[key];
}
