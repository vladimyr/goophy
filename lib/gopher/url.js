'use strict';

const GopherType = require('./type');
const NamedRegExp = require('named-regexp-groups');

const getter = fn => ({ get: fn, configurable: false, enumerable: true });
const reUrl = new NamedRegExp([
  '^(?<protocol>(?:web\\+)?gopher):\\/\\/',
  '(?<hostname>[^:/]+)(?::(?<port>\\d+))?',
  '(?:\\/(?<type>.)(?<selector>.*?)|\\/?)$'
].join(''));

class GopherUrl {
  constructor(url) {
    const match = reUrl.exec(url);
    if (!match) throw new Error('Invalid gopher url');
    const { groups } = match;
    const { port = 70 } = groups;
    Object.defineProperties(this, {
      protocol: getter(() => groups.protocol),
      hostname: getter(() => groups.hostname),
      port: getter(() => port),
      host: getter(() => `${groups.hostname}:${port}`),
      type: getter(() => GopherType.fromCode(match.groups.type)),
      selector: getter(() => groups.selector || ''),
      href: getter(() => url)
    });
  }

  toString() {
    return this.href;
  }
}

module.exports = GopherUrl;
