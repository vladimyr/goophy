'use strict';

module.exports = {
  send,
  sendHtml,
  sendText,
  sendCode
};

function sendHtml(res, html, { statusCode } = {}) {
  return send(res, html, {
    contentType: 'text/html; charset=utf-8',
    statusCode
  });
}

function sendText(res, text, { statusCode } = {}) {
  return send(res, text, {
    contentType: 'text/plain; charset=utf-8',
    statusCode
  });
}

function sendCode(res, statusCode, message = '') {
  return send(res, message, { statusCode });
}

function send(res, content, { contentType, statusCode = 200 } = {}) {
  if (contentType) res.setHeader('Content-Type', contentType);
  res.statusCode = statusCode;
  res.end(content);
}
