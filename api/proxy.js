'use strict';

const { render } = require('mustache');
const { sendHtml, sendText, sendCode } = require('../lib/server');
const { URLSearchParams } = require('url');
const debug = require('debug')('proxy');
const fileType = require('file-type');
const fs = require('fs');
const gopher = require('../lib/gopher');
const GopherType = require('../lib/gopher/type');
const path = require('path');
const pipe = require('pump');
const through2 = require('through2');

// Gopher page template
const readFile = path => fs.readFileSync(path, 'utf-8');
const template = readFile(path.join(__dirname, '../page.html'));
const renderPage = (ctx = {}) => render(template, ctx);

const getSearchParams = url => new URLSearchParams(url.split('?')[1]);
const normalizeUrl = (url = '') => url.replace(/gopher:?\/{1,2}/, 'gopher://');
const isText = type => type === GopherType.Menu || type === GopherType.Text;

module.exports = async (req, res) => {
  const searchParams = getSearchParams(req.url);
  const url = normalizeUrl(searchParams.get('url'));
  const raw = searchParams.has('raw');
  debug('raw? %s', raw);
  debug('Request url: %s', url);
  try {
    const gopherResp = await gopher(url);
    await send(res, gopherResp, { raw });
  } catch (err) {
    debug('error', err.stack);
    sendCode(res, 400, 'Bad Request');
  }
};

async function send(res, gopherResp, { raw = false } = {}) {
  const { type } = gopherResp;
  if (!isText(type)) {
    const defaultMimetype = type === GopherType.Binary && 'application/octet-stream';
    return sendStream(res, gopherResp.body, { defaultMimetype });
  }
  const content = await gopherResp.text();
  if (raw) {
    debug('Replying with plain text');
    return sendText(res, content);
  }
  debug('Replying with html');
  const html = renderPage({ url: gopherResp.url, title: '', content });
  return sendHtml(res, html);
}

function sendStream(res, body, { statusCode = 200, defaultMimetype } = {}) {
  res.statusCode = statusCode;
  return pipe(body, through2((chunk, _, cb) => {
    if (res.getHeader('Content-Type')) return cb(null, chunk);
    const { mime } = fileType(chunk) || { mime: defaultMimetype };
    if (!mime) {
      return cb(new Error('Failed to determine mimetype'));
    }
    debug('Response mimetype: %s', mime);
    res.setHeader('Content-Type', mime);
    cb(null, chunk);
  }), res);
}
